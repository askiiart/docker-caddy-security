# docker-caddy-security

caddy-security in Docker, updated daily.

[![Build Status](https://drone.askiiart.net/api/badges/askiiart/docker-caddy-security/status.svg?ref=refs/heads/main)](https://drone.askiiart.net/askiiart/docker-caddy-security)

Refer to the [Caddy docs](https://caddyserver.com/docs/) and [Caddy Security docs](https://authp.github.io/) for how to actually use this.
